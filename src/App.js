// Did not use useState since the data is not being updated.
import React from "react";
import data from "./data.js";
import SingleQuestion from "./Question";

const App = () => {
  return (
    <main>
      <div className="container">
        <h3>General Trivia</h3>
        <section className="info">
          {/* Each object in "data" is iterated over and returned as its own component.*/}
          {data.map((singleObject) => {
            return (
              <SingleQuestion
                key={singleObject.id}
                title={singleObject.title}
                info={singleObject.info}
              />
            );
          })}
        </section>
      </div>
    </main>
  );
};

export default App;
