const questions = [
  {
    id: 1,
    title: "What does WWW stand for in a web browser?",
    info: "World Wide Web",
  },
  {
    id: 2,
    title: "How long is an olympic simming pool (in meters)?",
    info: "50 meters",
  },
  {
    id: 3,
    title: "What countries made up the original Axis powers in World War II?",
    info: "Germany, Italy, and Japan",
  },
  {
    id: 4,
    title: "What geometric shape is generally used for stop signs?",
    info: "Octagon",
  },
  {
    id: 5,
    title: "Who was the first woman pilot to fly solo across the Atlantic?",
    info: "Amelia Earhart",
  },
];
export default questions;
