import React, { useState } from "react";
import { AiOutlineMinus, AiOutlinePlus } from "react-icons/ai";
// questionData holds the passed props from App.js in a single variable.
const Question = (questionData) => {
  const [isExpanded, setIsExpanded] = useState(false);

  // Destructure the props passed from App.js..
  const { info, title } = questionData;

  return (
    <article className="question">
      <header>
        <h4>{title}</h4>
        <button
          className="btn"
          onClick={() => {
            setIsExpanded(!isExpanded);
          }}
        >
          {isExpanded ? <AiOutlineMinus /> : <AiOutlinePlus />}
        </button>
      </header>
      <p>{isExpanded && info}</p>
    </article>
  );
};

export default Question;
