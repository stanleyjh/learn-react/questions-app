# [Trivia](https://stanleyjh.gitlab.io/learn-react/questions-app/)

This application shows a list of questions and answers to general trivia questions.

The data is referenced locally in the data.js file. Each object in the `data` array is iterated over using the map method. Each object is returned with the `SingleQuestion` component.

The props passed to the `SingleQuestion` component is then destructured to access the object's properties. Clicking the +/- button will hide or show the information. This is done with the State Hook. The `isExpanded` variable is assigned a `false` value by default and is flipped to `true` when the +/- button is clicked.

## References

[John Smilga's Udemy React Course](https://www.udemy.com/course/react-tutorial-and-projects-course/?referralCode=FEE6A921AF07E2563CEF)

[React Icons Library](https://react-icons.github.io/react-icons/)
